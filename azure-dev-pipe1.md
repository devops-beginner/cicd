## Creating the Dev Pipeline in Azure for Java Application
**Prerequisites**
- A project on a Version Control for which the Pipeline will be created.
- [Microsoft Azure](https://azure.microsoft.com/) or [Azure DevOps portal](https://dev.azure.com/) 

**Creating the Pipeline**
Following screenshots demonstrates the step by step actions to create a development pipeline to create a artifact.

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/1.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/2.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/3.png)
![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/4.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/5.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/6.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/7.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/8.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/9.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/10.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/11.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/12.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/13.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/14.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/15.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/16.png)

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-azure/p1/17.png)