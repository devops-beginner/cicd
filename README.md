## Continuous Integration & Continuous Delivery

Small + Fast = Better
Continuous integration is the practice of automatically building and unit testing an entire application frequently, ideally on every source code check-in— dozens of times a day if necessary.

Continuous delivery is the additional practice of deploying every change to a production-like environment and performing automated integration and acceptance testing after it passes its build and unit tests.

Continuous deployment extends this concept to where every change goes through full automated testing and is deployed automatically to the production environment.

**CICD Workflow**

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/cicd-workflow.png)

CD pipelines consist of the following components.
-  Source code repository 
-  Build server 
-  Unit tests
-  Artifact repository
-  Deployer 
-  Integration tests 
-  End-to-end tests 
-  Security (and other specialized) tests

**Continuous Integration**
- Continuous Integration Practices. Below diagram shows CI workflow.

![enter image description here](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/CI-Workflow.png)


**Testing**
Unit testing is performed at build time on a single unit of code and/or artifact without use of external dependencies or deployment.

Integration testing is performed as you bring together pieces of your application and as it needs to use external dependencies—databases—to actually do its thing.

End-to-end testing, often implemented as UI testing, is when you test more of your application stack in the way an end user actually does.

Security testing looks for flaws in your code and runtime to prevent compromises and leaking of data in production.

TDD, or test-driven development, is the practice of writing a failing test first, and then writing the code that causes the test to pass, and then refactoring it to make it cleaner.

BDD, or behavior-driven development, is a refinement of TDD that focuses on simple sentence-driven testing.

ATDD, or acceptance test-driven development, extends this to where the project team decides on a set of BDD acceptance tests before development begins.

**The Continuous Delivery Pipeline**
To successfully perform continuous delivery

1.  Only build artifacts once.
2.  Artifacts should be immutable.
3.  Deployment should go to a copy of production before going into production.
4.  Stop deploys if it a previous step fails.
5.  Deployments should be idempotent.

- Each developer is responsible for their check-in through deployment
    
-  Small changes. Build quality in
    
-   Don’t check in on broken builds
    
    -  Take responsibility for your build
        
    - Immediately address a broken build
        
    - Revert if fixing takes time
        
    - No check-ins while the build is broken— the line stops
        
-  Automate high-quality testing
    
    -  Run tests before check-in
        
    - Fix flaky tests
        
    - Don’t ignore or disable tests
        
-  Automate deployment
    
-  Keep the build and deploy fast
    
-   Balance your testing

**Git:** `git log` shows all the commits/ history of the repo.

`git show <commit-id>` shows the commit description.

`git diff` shows what has changed.

Git *pre commit* hooks are set of actions that will be run before proceeding. Hooks are optional

*Webhooks* allow us to integrate third party applications which will be notified whenever there has been a change in the repository. 
